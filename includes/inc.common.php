<?php
session_start();

spl_autoload_register(function ($name) {
    if(file_exists("includes/classes/".$name.".php")){
		require_once("includes/classes/".$name.".php");
	}
});


$tmpl = new Template;
$cfg = new Config($tmpl);

$tmpl->addStyle("/includes/styles/main.css");
$tmpl->setTitle($cfg->siteTitle);

?>