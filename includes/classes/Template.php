<?php

class Template extends Debug {
	private $content = "";
	private $styles = array();
	private $title = "";
	private $subTitle = "";
	public $cfg = NULL;
	
	/**
	 * initializes the class
	 *
	 * @param String $template it's for future things - currently not necessary
	 * @return void
	 */
	function __construct($template = "main"){
		parent::__construct();
				
		set_error_handler([$this, "errorHandler"]);
		set_exception_handler([$this, "exceptionHandler"]);
	}
	
	/*
	 * Destructor that prints the Debugging and replaces the placeholders in the template
	 */
	function __destruct(){
		$this->prnt(parent::showDebug());
		
		$file = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/includes/templates/main.tpl');
		echo preg_replace_callback("/%%(.*)%%/im", "self::replaceTemplateVars", $file);
	}
	
	/*
	 * Method to print some content
	 *
	 * @param String $content - the content which should be displayed
	 * @return void
	 */
	public function prnt($content){
		$this->content .= $content;	
	}
	
	/**
	 * Sets the current page title
	 *
	 * @param String $title the title
	 * @return void
	 */
	public function setTitle($title){
		$this->title = $title;
	}
	
	/** 
	 * Sets the subtitle of the current page
	 *
	 * @param String $title the subtitle
	 * @return void
	 */
	public function setSubtitle($title){
		$this->subTitle = $title;
	}
	
	/**
	 * adds a css file to the array
	 *
	 * @param String $filePath the path to the css file
	 * @return void
	 */
	public function addStyle($filePath){
		$this->styles[] = $filePath;
	}
	
	/**
	 * called in the Destructor to replace the placeholder with its values
	 *
	 * @param String $var the placeholder
	 * @return String
	 */
	private function replaceTemplateVars($var){
		switch($var[1]){
			case "content": return $this->content; break;
			case "styles": return $this->getParsedStyles(); break;
			case "page-title": return $this->title.$this->subTitle; break;
			default: return ""; break;
		}
	}
	
	/**
	 * parses the css files to html code
	 *
	 * @return String
	 */
	private function getParsedStyles(){
		$styles = "";
		
		foreach($this->styles as $style){
			$styles .= '<link rel="stylesheet" href="'.$style.'" />'."\n";
		}
		
		return $styles;
	}
}
?>