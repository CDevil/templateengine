<?php
class Debug {
	private $debugMessages = [];
	
	function __construct(){}
	
	function __destructor(){
		$this->showDebug();
	}
	
	/**
	 * Method to show the debug messages if there are any messages
	 *
	 * @return String the formatted output
	 */
	public function showDebug(){
		if(count($this->debugMessages) > 0){
			$return = '
			<div id="dev-frame">
				<div id="dev-content">';
				
				foreach($this->debugMessages as $m){
					$return .= '<div class="dev-error '.$m['type'].'">'.str_replace($_SERVER['DOCUMENT_ROOT'], "", $m['message']).'</div>';
				} 
			
				$return .='
				</div>
			</div>';
			
			return $return;
		}
	}
	
	/**
	 * Adds a debug message to the array
	 *
	 * @param String $message the message itself
	 * @param String $type the type of the message (e.g. error, notice)
	 * @return void
	 */
	public function addDebug($message, $type = "notice"){
		$this->debugMessages[] = ["type" => $type, "message" => $message];
	}
	
	/**
	 * Handles the occuring runtime errors sent from php
	 */
	public function errorHandler($no, $str, $file, $line){
		switch($no){
			case 1: $type = "error"; break;
			case 2: case 4: $type = "warning"; break;
			case 8: $type = "notice"; break;
			default: $type = "error"; break;
		}
		
		$format = $file.' : '.$line.'<br />'.$str;
		$this->addDebug($format, $type);
		
		return true;
	}
	
	/** 
	 * Handles the occuring exceptions sent from php
	 */
	public function exceptionHandler($exception){
		
		$format = $exception->getFile().' : '.$exception->getLine().'<br />'.$exception->getMessage();
		$this->addDebug($format, "error");
		
		return true;
	}
}
?>