<?php

class Config {
	private $tmpl = null;
	private $configFile = "includes/inc.config.php";
	private $configArray = null;
	
	/**
	 * Constructor to initialize the class
	 *
	 * @param Template $tmpl holds an instance of the Tempalte-class
	 * @param String $configFile a string representing the path to the config file
	 * @return void
	 */
	function __construct($tmpl = NULL, $configFile = NULL){
		if(isset($tmpl)){
			$this->tmpl = $tmpl;
			
			if($configFile !== NULL){
				$this->configFile = $configFile;
			}
			
			if(file_exists($this->configFile)){
				$this->configArray = include($this->configFile);
			} else {
				throw new Exception("Config wurde nicht gefunden");
			}
		} else {
			throw new Exception("Es wurde keine Template-Instanz übergeben");
		}
	}
	
	/**
	 * Magic method for getting a value out of the config array 
	 *
	 * @param String $name the name of the index
	 * return String
	 */
	public function __get($name){
		if(array_key_exists($name, $this->configArray)){
			return $this->configArray[$name];
		} else {
			$bt = debug_backtrace();
			$bt = array_shift($bt);
			$this->tmpl->addDebug($bt['file'].' : '.$bt['line'].'<br />Fehler beim Zugriff auf Variable, "'.$name.'" wurde nicht gefunden.', "warning");
		}
	}
	
	/**
	 * a print method that prints out the config array
	 * it's just for testing
	 */
	public function printConfig(){
		$this->tmpl->prnt("Template");
		$this->tmpl->prnt(print_r($this->configArray, true));
	}
}
?>